const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  auth: state => state.user.auth,
  session_routes: state => state.user.session_routes,
  introduction: state => state.user.introduction,
  roles: state => state.user.roles,
  permission_routes: state => state.permission.routes,
  errorLogs: state => state.errorLog.logs,
  administrador_consorcios: state => state.administrador.consorcios,
  consorcio: state => { return idConsorcio => 
    state.administrador.consorcios.find(consorcio => consorcio.idConsorcio === idConsorcio)
  }
  ,
  departamento: state => { return idDepartamento => 
     state.administrador.departamentos.find(departamento => departamento.idDepartamento === idDepartamento)
  }
  
}
export default getters
