import { getConsorcios, addConsorcio, updateConsorcio } from '@/api/consorcios'
import Vue from 'vue'

const state = {
  listado: []
}

const mutations = {
  SET_CONSORCIOS: (state, consorcios) => {
    state.listado = consorcios
  },
  ADD_CONSORCIO: (state, consorcio) => {
    state.listado.push(consorcio)
  },
  UPDATE_CONSORCIO: (state, consorcio) => {
    const pos = state.listado.findIndex(viejoConsorcio => viejoConsorcio.idConsorcio === consorcio.idConsorcio)
    console.log(pos)
    Vue.set(state.listado, pos, consorcio)
  }

}

const actions = {
  getConsorcios({ commit, state }) {
    return new Promise((resolve, reject) => {
      getConsorcios().then(response => {
        commit('SET_CONSORCIOS', response)
        resolve()
      }).catch(error =>
        reject(error))
    })
  },

  addConsorcio({ commit, state }, consorcio) {
    return new Promise((resolve, reject) => {
      addConsorcio(consorcio).then(response => {
        commit('ADD_CONSORCIO', response)
        resolve()
      }).catch(error =>
        reject(error))
    })
  },

  editConsorcio({ commit }, consorcio) {
    return new Promise((resolve, reject) => {
      updateConsorcio(consorcio).then(response => {
        commit('UPDATE_CONSORCIO', response)
        resolve()
      }).catch(error =>
        reject(error))
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
