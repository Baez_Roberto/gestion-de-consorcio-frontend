import {
  getPerfil,
  getConsorcio,
  getConsorcios,
  updateConsorcio,
  getDepartamentos,
  getDepartamento,
  addDepartamento,
  addVinculo,
  addVinculoUsuario,
  getUsuariosByVinculo,
  getVinculosUsuariosByVinculo,
  getDepartamentoUltimoVinculo,
  getMensajesTablon
} from '@/api/administrador'
import { getUsuarioByEmail } from '@/api/usuarios'
import Vue from 'vue'

const state = {
  administrador: {},
  consorcios: [],
  departamentos: [],
  usuarios: [],
  mensajes_tablon: [],
  vinculo: {},
  vinculos_usuarios: []

}

const mutations = {
  SET_MENSAJES_TABLON: (state, mensajes_tablon) => {
    state.mensajes_tablon = mensajes_tablon
  },

  SET_CONSORCIOS: (state, consorcios) => {
    state.consorcios = consorcios
  },
  SET_ADMINISTRADOR: (state, administrador) => {
    state.administrador = administrador
  },
  UPDATE_CONSORCIO: (state, consorcio) => {
    const pos = state.consorcios.findIndex(viejoConsorcio => viejoConsorcio.idConsorcio === consorcio.idConsorcio)
    Vue.set(state.consorcios, pos, consorcio)
  },
  SET_DEPARTAMENTOS: (state, departamentos) => {
    state.departamentos = departamentos
  },
  UPDATE_DEPARTAMENTO: (state, departamento) => {
    const pos = state.departamentos.findIndex(viejoDepto => viejoDepto.idDepartamento === departamento.idDepartamento)
    if (pos === -1) {
      state.departamentos.push(departamento)
    } else {
      Vue.set(state.departamentos, pos, departamento)
    }
  },
  ADD_DEPARTAMENTO: (state, departamento) => {
    state.departamentos.push(departamento)
  },
  SET_USUARIOS: (state, usuarios) => {
    state.usuarios = usuarios
  },
  UPDATE_USUARIO: (state, usuario) => {
    const pos = state.usuarios.findIndex(viejoUsuario => viejoUsuario.idUsuario === usuario.idUsuario)
    if (pos < 0) {
      state.usuarios.push(usuario)
    } else {
      Vue.set(state.usuarios, pos, usuario)
    }
  },
  SET_VINCULO: (state, vinculo) => {
    state.vinculo = vinculo
  },
  SET_VINCULOS_USUARIOS: (state, vinculos_usuarios) => {
    state.vinculos_usuarios = vinculos_usuarios
  },
  ADD_VINCULO_USUARIO: (state, vinculo_usuario) => {
    state.vinculos_usuarios.push(vinculo_usuario)
  }
}

const actions = {
  // region CONSORCIOS
  getConsorcios({ commit, state }) {
    return new Promise((resolve, reject) => {
      getConsorcios().then(response => {
        commit('SET_CONSORCIOS', response)
        resolve(response)
      }).catch(error =>
        reject(error))
    })
  },
  getConsorcio({ commit }, idConsorcio) {
    return new Promise((resolve, reject) => {
      getConsorcio(idConsorcio).then(response => {
        commit('UPDATE_CONSORCIO', response)
        resolve(response)
      }).catch(error =>
        reject(error))
    })
  },
  editConsorcio({ commit }, consorcio) {
    return new Promise((resolve, reject) => {
      updateConsorcio(consorcio).then(response => {
        commit('UPDATE_CONSORCIO', response)
        resolve()
      }).catch(error =>
        reject(error))
    })
  },
  // endregion
  // region DEPARTAMENTOS
  getDepartamentos({ commit }, idConsorcio) {
    return new Promise((resolve, reject) => {
      getDepartamentos(idConsorcio).then(response => {
        commit('SET_DEPARTAMENTOS', response)
        resolve()
      }).catch(error =>
        reject(error))
    })
  },

  getMensajesTablon({ commit }, idConsorcio) {
    return new Promise((resolve, reject) => {
      getMensajesTablon(idConsorcio).then(response => {
        commit('SET_MENSAJES_TABLON', response)
        resolve()
      }).catch(error =>
        reject(error))
    })
  },


  getDepartamento({ commit }, { idConsorcio, idDepartamento }) {
    return new Promise((resolve, reject) => {
      getDepartamento(idConsorcio, idDepartamento).then(response => {
        commit('UPDATE_DEPARTAMENTO', response)
        resolve(response)
      }).catch(error =>
        reject(error))
    })
  },
  addDepartamento({ commit }, { consorcio, departamento }) {
    return new Promise((resolve, reject) => {
      addDepartamento(consorcio, departamento).then(response => {
        commit('ADD_DEPARTAMENTO', response)
        resolve()
      }).catch(error =>
        reject(error))
    })
  },
  // endregion
  // region VINCULOS
  getDepartamentoUltimoVinculo({commit}, { idConsorcio, idDepartamento }) {
    return new Promise((resolve, reject) => {
      getDepartamentoUltimoVinculo(idConsorcio, idDepartamento).then(response => {
        commit('SET_VINCULO', response)
        resolve(response)
      }).catch(error =>
        reject(error))
    })
  },
  addVinculo({commit}, {idConsorcio, idDepartamento}) {
    return new Promise( (resolve, reject) => {
      addVinculo(idConsorcio, idDepartamento).then(response => {
        commit('SET_VINCULO', response)
        resolve(response)
      }).catch(error =>
      reject(error))
    })
  },
  getUsuariosByVinculo({ commit }, { idConsorcio, idDepartamento, idVinculo }) {
    return new Promise((resolve, reject) => {
      getUsuariosByVinculo(idConsorcio, idDepartamento, idVinculo).then(response => {
        commit('SET_USUARIOS', response)
        resolve(response)
      }).catch(error =>
        reject(error))
    })
  },
  getVinculosUsuariosByVinculo({ commit }, { idConsorcio, idDepartamento, idVinculo }) {
    return new Promise((resolve, reject) => {
      getVinculosUsuariosByVinculo(idConsorcio, idDepartamento, idVinculo).then(response => {
        commit('SET_VINCULOS_USUARIOS', response)
        resolve(response)
      }).catch(error =>
        reject(error))
    })
  },
  addVinculoUsuario({commit}, {idConsorcio, idDepartamento, idVinculo, vinculoUsuario}) {
    return new Promise((resolve, reject) => {
      addVinculoUsuario(idConsorcio, idDepartamento, idVinculo, vinculoUsuario).then(response => {
        commit('ADD_VINCULO_USUARIO', response)
        resolve()
      }).catch(error =>
      reject(error))
    })
  },
  // endregion
  getPerfil({ commit }) {
    return new Promise((resolve, reject) => {
      getPerfil().then(response => {
        commit('SET_ADMINISTRADOR', response)
        resolve()
      }).catch(error =>
        reject(error))
    })
  },
  getUsuarioByEmail({ commit }, email) {
    return new Promise((resolve, reject) => {
      getUsuarioByEmail(email).then(response => {
        commit('UPDATE_USUARIO', response)
        resolve(response)
      }).catch(error =>
        reject(error))
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
