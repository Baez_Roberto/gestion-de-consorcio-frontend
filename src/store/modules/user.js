import { login, perfil } from '@/api/cuenta'
import { getToken, getAuth, setAuth, setToken, removeToken, removeAuth } from '@/utils/auth'
import router, { resetRouter } from '@/router'

const state = {
  auth: getAuth(),
  name: '',
  avatar: '',
  introduction: '',
  roles: [],
  token: '',
  session_routes: false
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_AUTH: (state, { username, password }) => {
    state.auth = { username: username, password: password }
  },
  SET_INTRODUCTION: (state, introduction) => {
    state.introduction = introduction
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_SESSION_ROUTES: (state, value) => {
    state.session_routes = value
  }
}

const actions = {
  // user login
  login({ commit }, { username, password, rol }) {
    username = ''.concat(username, '/', rol)
    return new Promise((resolve, reject) => {
      login(username, password).then(response => {
        commit('SET_AUTH', { username, password })
        setAuth({ username, password, rol })
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      perfil().then(response => {
        if (!response) {
          reject('Verification failed, please Login again.')
        }
        const { nombre, roles } = response
        commit('SET_NAME', nombre)
        commit('SET_ROLES', roles)
        resolve({ roles: roles })
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      commit('SET_AUTH', {})
      removeToken()
      removeAuth()
      resetRouter()
      resolve()
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      commit('SET_AUTH', {})
      removeToken()
      removeAuth()
      resolve()
    })
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
