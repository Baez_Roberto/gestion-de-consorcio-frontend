import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */

import { getNombreConsorcio } from '@/router/utils'

export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login/:rol',
    component: () => import('@/views/login/index'),
    hidden: true,
    props: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',

    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Panel de control',
        meta: { title: 'Panel de control', icon: 'dashboard', roles: ['ROLE_SUPERUSUARIO'] },
        hidden: true
      }
    ]
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */

export const asyncRoutes = [
  {
    path: '/administradores',
    component: Layout,
    redirect: '/administradores/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/permission/administradores'),
        name: 'Administradores',
        meta: { title: 'Administradores', icon: 'user', roles: ['ROLE_SUPERUSUARIO'] }
      }
    ]
  },
  {
    path: '/consorcios',
    component: Layout,
    redirect: '/consorcios/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/permission/consorcios'),
        name: 'Consorcios',
        meta: { title: 'Consorcios', icon: 'tab', roles: ['ROLE_SUPERUSUARIO'] }
      }
    ]
  },

  {
    path: '/tablon-mensajes',
    component: Layout,
    redirect: '/tablon-mensajes/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/administrador/tablon-mensajes/tablon'),
        name: 'Tablon Mensajes',
        meta: { title: 'Tablon de Mensajes', icon: 'table', roles: ['ROLE_ADMINISTRADOR'] },
        
      },
      {
        path: 'detalles/:idmensaje',
        component: () => import('@/views/administrador/tablon-mensajes/detalles'),
        name: 'DetalleMensaje',
        meta: { title: 'Detalle Mensaje Tablon', noCache: true, activeMenu: '/tablon-mensajes/index' },
        hidden: true
      }
    ]
  },

  {
    path: '/Reservas',
    component: Layout,
    redirect: '/Reservas/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/administrador/reservas/reservas_index'),
        name: 'Reservas',
        meta: { title: 'Reservas', icon: 'people', roles: ['ROLE_ADMINISTRADOR'] },
        
      },
      {
        path: 'detallesReserva/:idReserva',
        component: () => import('@/views/administrador/reservas/detalles'),
        name: 'DetalleReserva',
        meta: { title: 'Detalle Reserva', noCache: true, activeMenu: '/Reserva/index' },
        hidden: true
      }
    ]
  },

  {
    path: '/mensajes_directos',
    component: Layout,
    redirect: '/mensajes_directos/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/administrador/mensajes_directos/mensajes'),
        name: 'Chat',
        meta: { title: 'Chat', icon: 'wechat', roles: ['ROLE_ADMINISTRADOR'] },
        
      },
      {
        path: 'detallesChat/:idChat',
        component: () => import('@/views/administrador/mensajes_directos/detalles'),
        name: 'DetalleMensaje',
        meta: { title: 'Detalle Chat', noCache: true, activeMenu: '/mensajes_directos/index' },
        hidden: true
      }
    ]
  },


  {
    path: '/consorcios',
    component: Layout,
    name: 'bubu',
    meta: { title: 'Consorcios', icon: 'tab', roles: ['ROLE_ADMINISTRADOR'] },
    children: [
      {
        path: `/consorcios/:idConsorcio`,
        component: () => import('@/views/administrador/consorcio/index'),
        redirect: `/consorcios/:idConsorcio/index`,
        props: (route) => {
          const idConsorcio = Number.parseInt(route.params.idConsorcio)
          return { idConsorcio }
        },
        meta: {
          title: "{ let co = this.$store.state.administrador.consorcios.find(c => c.idConsorcio == this.$route.params.idConsorcio); `${co.nombreConsorcio}`}",
          eval: true,
          sidebarList: "{this.$store.state.administrador.consorcios}"
        },
        children: [
          {
            hidden: true,
            path: `index`,
            component: () => import('@/views/administrador/consorcio/index'),
            props: (route) => {
              const idConsorcio = Number.parseInt(route.params.idConsorcio)
              return { idConsorcio }
            }
          },
          {
            hidden: true,
            meta: { title: 'Propiedades' },
            path: `propiedades`,
            redirect: `propiedades/index`,
            component: () => import('@/views/administrador/consorcio/propiedades'),
            props: (route) => {
              const idConsorcio = Number.parseInt(route.params.idConsorcio)
              return { idConsorcio }
            },
            children: [
              {
                path: `index`,
                hidden: true,
                component: () => import('@/views/administrador/consorcio/propiedades/index'),
                props: (route) => {
                  const idConsorcio = Number.parseInt(route.params.idConsorcio)
                  return { idConsorcio }
                },
              },
              {
                hidden: true,
                path: `tipos_departamentos`,
                component: () => import('@/views/administrador/consorcio/propiedades/cfg_tipo_departamento'),
                props: (route) => {
                  const idConsorcio = Number.parseInt(route.params.idConsorcio)
                  return { idConsorcio }
                },
              }]
          },
          {
            meta: {
              title: "{ let de = this.$store.state.administrador.departamentos.find(d => d.idDepartamento == this.$route.params.idDepartamento); `${de.piso} - ${de.identificador}`}",
              eval: true,
            },
            hidden: true,
            path: `departamentos/:idDepartamento`,
            component: () => import('@/views/administrador/consorcio/propiedades'),
            redirect: `departamentos/:idDepartamento/index`,
            props: (route) => {
              const idDepartamento = Number.parseInt(route.params.idDepartamento)
              const idConsorcio = Number.parseInt(route.params.idConsorcio)
              return { idConsorcio, idDepartamento }
            },
            children: [
              {
                hidden: true,
                path: `index`,
                component: () => import('@/views/administrador/consorcio/departamento'),
                props: (route) => {
                  const idDepartamento = Number.parseInt(route.params.idDepartamento)
                  const idConsorcio = Number.parseInt(route.params.idConsorcio)
                  return { idDepartamento, idConsorcio }
                },
              }]
          },
        ]
      }
    ]
  },

  /** when your routing map is too long, you can split it into small modules **/
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
