import defaultSettings from '@/settings'

const title = defaultSettings.title || 'Consorcio Interactivo'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
