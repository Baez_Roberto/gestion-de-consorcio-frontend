import Cookies from 'js-cookie'
import service from '@/utils/request_roberto'

const TokenKey = 'Token'
const UsernameKey = 'Username'
const PassworkKey = 'Password'
const RolKey = 'Rol'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken({ username, password }) {
  Cookies.set(UsernameKey, username)
  Cookies.set(PassworkKey, password)
}

export function setAuth({ username, password, rol }) {
  removeAuth()
  Cookies.set(UsernameKey, username)
  Cookies.set(PassworkKey, password)
  Cookies.set(RolKey, rol)
}

export function getRol() {
  return Cookies.get(RolKey)
}

export function getAuth() {
  var username = Cookies.get(UsernameKey)
  var password = Cookies.get(PassworkKey)
  return { username: username, password: password }
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function removeAuth() {
  Cookies.remove(UsernameKey)
  Cookies.remove(PassworkKey)
  return
}

export function isAuthValid(auth) {
  if (auth.hasOwnProperty('username') && auth.username &&
      auth.hasOwnProperty('password') && auth.password) {
    return true
  }
  return false
}
