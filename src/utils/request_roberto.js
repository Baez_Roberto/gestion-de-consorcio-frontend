import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken, getAuth, isAuthValid } from '@/utils/auth'

// create an axios instance
const service = axios.create({
  baseURL: 'http://localhost:8080', // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
  // auth: { username: 'test', password: 'test'}
})

// request interceptor
service.interceptors.request.use(
  config => {
    const auth = store.getters.auth
    // do something before request is sent
    if (isAuthValid(auth)) {
      config.auth = getAuth()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data

    // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
    if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
      // to re-login
      MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
        confirmButtonText: 'Re-Login',
        cancelButtonText: 'Cancel',
        type: 'warning'
      }).then(() => {
        store.dispatch('user/resetToken').then(() => {
          location.reload()
        })
      })
    }

    return res
  },
  error => {
    if (error.response.status == 401) {
      if (isAuthValid(store.getters.auth)) {
        /* Si se da un error 401 en cualquier pagina habiendo estado logueado
        * significa que hubo un problema en la autenticacion, se cierra la
        * sesion y se redigire al usuario a la pantalla de inicio.
        */
        MessageBox.confirm('Oops, ha ocurrido un error, vuelve a iniciar sesión', 'Sesión terminada', {
          confirmButtonText: 'Aceptar',
          type: 'error',
          showCancelButton: false,
          showClose: false,
          closeOnPressEscape: false,
          closeOnClickModal: false
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
            return Promise.reject(error)
          })
        })
      } else {
        /* Se dio un 401 pero el usuario nunca se logueo, significa que
        * esta en la pantalla de login y las credenciales ingresadas no son
        * validas. Se reenvia el error a la pantalla de login para que
        * lo maneje
        */
        return Promise.reject(error)
      }
    } else {
      console.log('err' + error) // for debug
      return Promise.reject(error)
    }
  }
)

export default service
