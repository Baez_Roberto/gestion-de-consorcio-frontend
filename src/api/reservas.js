import request from '@/utils/request_roberto'


export function getReservasById(idReserva) {
    return request({
      url: `/reservas/id/${idReservas}`,
      method: 'get'
    })
}

export function getReservas() {
  return request({
    url: '/reservas',
    method: 'get'
  })
}