import request from '@/utils/request_roberto'

export function getUsuarioByEmail(email) {
  return request({
    url: '/usuarios',
    method: 'get',
    params: {
      correo: email
    }
  })
}

export function getUsuarios() {
  return request({
    url: '/usuarios',
    method: 'get'
  })
}

export function addUsuario(data) {
  return request({
    url: '/persistirUsuario',
    method: 'post',
    data
  })
}

export function deleteUsuario(login) {
  return request({
    url: `/borrarUsuario/${login} `,
    method: 'delete'
  })
}

export function getUsuariosByNombre(nombre) {
  return request({
    url: `/usuariosByNombre/${nombre}`,
    method: 'get'
  })
}

export function getUsuariosByEmail(email) {
  return request({
    url: `/usuariosByEmail/${email}`,
    method: 'get'
  })
}

export function getUsuariosByTelefono(telefono) {
  return request({
    url: `/usuariosByTelefono/${telefono}`,
    method: 'get'
  })
}
