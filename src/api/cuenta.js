import request from '@/utils/request_roberto'

export function login(username, password) {
  return request({
    url: '/cuenta/login',
    method: 'get',
    auth: { username: username, password: password }
  })
}

export function perfil() {
  return request({
    url: '/cuenta/perfil',
    method: 'get'
  })
}
