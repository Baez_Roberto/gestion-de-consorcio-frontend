import request from '@/utils/request_roberto'

export function getPerfil() {
  return request({
    url: '/administradores/me',
    method: 'get'
  })
}

// region CONSORCIO
export function getConsorcios() {
  return request({
    url: '/administradores/me/consorcios',
    method: 'get'
  })
}

export function getConsorcio(idConsorcio) {
  return request({
    url: `/administradores/me/consorcios/${idConsorcio}`,
    method: 'get'
  })
}

export function updateConsorcio(consorcio) {
  return request({
    url: '/administradores/me/consorcios/actualizar',
    method: 'put',
    data: consorcio
  })
}
// endregion

// region DEPARTAMENTO
export function getDepartamentos(idConsorcio) {
  return request({
    url: `/administradores/me/consorcios/${idConsorcio}/departamentos`,
    method: 'get'
  })
}

export function getMensajesTablon(idConsorcio) {
  return request({
    url: `/administradores/me/consorcios/${idConsorcio}/mensajesTablon`,
    method: 'get'
  })
}

export function getDepartamento(idConsorcio, idDepartamento) {
  return request({
    url: `/administradores/me/consorcios/${idConsorcio}/departamentos/${idDepartamento}`,
    method: 'get'
  })
}

export function addDepartamento(consorcio, departamento) {
  return request({
    url: `/administradores/me/consorcios/${consorcio.idConsorcio}/departamentos/nuevo`,
    method: 'post',
    data: departamento
  })
}
// endregion

// region VINCULOS
export function getDepartamentoUltimoVinculo(idConsorcio, idDepartamento) {
  return request({
    url: `/administradores/me/consorcios/${idConsorcio}/departamentos/${idDepartamento}/vinculos/ultimo`,
    method: 'get'
  })
}

export function addVinculo(idConsorcio, idDepartamento) {
  return request({
    url:`/administradores/me/consorcios/${idConsorcio}/departamentos/${idDepartamento}/vinculos/nuevo`,
    method: `post`
  })
}

export function getUsuariosByVinculo(idConsorcio, idDepartamento, idVinculo) {
  return request({
    url: `/administradores/me/consorcios/${idConsorcio}/departamentos/${idDepartamento}/vinculos/${idVinculo}/usuarios`,
    method: 'get'
  })
}

export function getVinculosUsuariosByVinculo(idConsorcio, idDepartamento, idVinculo) {
  return request({
    url: `/administradores/me/consorcios/${idConsorcio}/departamentos/${idDepartamento}/vinculos/${idVinculo}/vinculos_usuarios`,
    method: 'get'
  })
}

export function addVinculoUsuario(idConsorcio, idDepartamento, idVinculo, vinculoUsuario) {
  return request({
    url: `/administradores/me` +
    `/consorcios/${idConsorcio}` +
    `/departamentos/${idDepartamento}` +
    `/vinculos/${idVinculo}` +
    `/vinculos_usuarios/agregar`,
    method: `post`,
    data: vinculoUsuario
  })
}
// endregion
