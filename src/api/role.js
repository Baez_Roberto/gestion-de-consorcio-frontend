import request from '@/utils/request_roberto'


export function getRoles() {
  return request({
    url: '/roles',
    method: 'get'
  })
}

export function addRole(data) {
  return request({
    url: '/persistirRol',
    method: 'post',
    data
  })
}


export function deleteRole(id) {
  return request({
    url:  `/borrarRol/${id} `,
    method: 'delete'
  })
}
