import request from '@/utils/request_roberto'

export function getAdministradores() {
  return request({
    url: '/administradores',
    method: 'get'
  })
}

export function addAdministrador(data) {
  return request({
    url: '/persistirAdministrador',
    method: 'post',
    data
  })
}

export function getAdministradorByNombre(nombre) {
  return request({
    url: `/administradorByNombre/${nombre}`,
    method: 'get'
  })
}

export function getAdministradoresByEmail(email) {
  return request({
    url: `/administradoresByEmail/${email}`,
    method: 'get'
  })
}

export function getAdministradoresByTelefono(telefono) {
  return request({
    url: `/administradoresByTelefono/${telefono}`,
    method: 'get'
  })
}

export function disableAdministradores(id) {
  return request({
    url: `/bloquearAdministrador/${id} `,
    method: 'put'
  })
}
