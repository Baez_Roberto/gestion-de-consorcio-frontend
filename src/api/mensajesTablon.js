import request from '@/utils/request_roberto'


export function getMensajeTablonById(idMensaje) {
    return request({
      url: `/mensajesTablon/id/${idMensaje}`,
      method: 'get'
    })
}

export function getMensajesTablon() {
  return request({
    url: '/mensajesTablon',
    method: 'get'
  })
}