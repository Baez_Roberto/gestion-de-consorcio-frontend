import request from '@/utils/request_roberto'

export function getConsorcios() {
  return request({
    url: '/consorcios',
    method: 'get'
  })
}

export function addConsorcio(consorcio) {
  return request({
    url: '/persistirConsorcio',
    method: 'post',
    data: consorcio
  })
}

export function deleteConsorcio(id) {
  return request({
    url: `/borrarConsorcio/${id} `,
    method: 'delete'
  })
}

export function updateConsorcio(consorcio) {
  return request({
    url: '/actualizarConsorcio',
    method: 'put',
    data: consorcio
  })
}
